class RegisterForm{
    constructor(selector,userService){
        this.selector = selector;
        this.userService = userService; 
        this.onRegister = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.conteiner = document.querySelector(this.selector);
        this.loginInput = this.conteiner.querySelector('.form_reg-input_login');
        this.passwordInput = this.conteiner.querySelector('.form_reg-input_password');
        this.dateInput = this.conteiner.querySelector('.form_reg-input_date');
        this.regBtn = this.conteiner.querySelector('.btn_reg');
    }
    binds(){
        this.regBtn.addEventListener('click',()=>this.register());
    }
    register(){
        let user = new User(
            this.loginInput.value,
            this.dateInput.value,
            this.passwordInput.value
        );
        this.userService.register(user).then(r=>{
            if(r.status === "error") this.registerError(r.error);
            else this.registerSuccess();
        })
    }
    registerError(text){
        alert(text);
    }
    registerSuccess(){
        this.clearForm();
        this.onRegister();
    }
    clearForm(){
            this.loginInput.value=''
            this.dateInput.value=''
            this.passwordInput.value=''
    }
}