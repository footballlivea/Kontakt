class AddContact{
    constructor(selector,conServ){
        this.selector = selector;
        this.conServ = conServ; 
        this.onAdd = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.conteiner = document.querySelector(this.selector);
        this.name = this.conteiner.querySelector('.form_add-input_name');
        this.type = this.conteiner.querySelector('.form_add-input_type');;
        this.value = this.conteiner.querySelector('.form_add-input_text');
        this.addBtn = this.conteiner.querySelector('button');
    }
    binds(){
        this.addBtn.addEventListener('click',()=>this.add());
    }
    add(){
        if(this.value.value==''||this.name.value==''||this.type.value==''){
                alert('Заполните поля ввода')
            }else{
                 let con = new Contact(
            this.value.value,
            this.name.value,
            this.type.value,
        );
        this.conServ.addContact(con).then(r=>{
            if(r.status === "error") this.addError(r.error);
            else this.addSuccess();
        })
            }
       
    }
    addError(text){
        alert(text);
    }
    addSuccess(){
        this.clearForm();
        this.onAdd();
    }
    clearForm(){
            this.name.value=''
            this.value.value=''
    }
}