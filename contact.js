class Contact{
    constructor(value,name,type,id){
        this.value = value;
        this.name = name;
        this.type = type;
        this.id = id;
    }
    create(c){
        let conElem = document.createElement('div');
        conElem.classList.add('con');

        if(c.id==activeContact){
            conElem.classList.add('active');
        }

        conElem.dataset.index=c.id;
    
        let showInf = document.createElement('div');
        showInf.innerHTML=">";
        showInf.classList.add('showBtn');
    
        conElem.append(`${c.name}`);
        conElem.append(showInf);
        
       return conElem;
    }

    showContact(){
        document.querySelector('.contact').innerHTML='';
        let elem = this.contact.map((c)=>this.create(c));
        document.querySelector('.contact').append(...elem);
    }
    createInfo(c){
        let conElem = document.createElement('div');
        conElem.classList.add('inf');

        if(c.id==activeContact){
            conElem.classList.remove('inf');
            conElem.classList.add('activeInf');
        }

       
    
        let name = document.createElement('div');
        name.innerHTML='Name: '+c.name;
        name.classList.add('name');

        let type = document.createElement('div');
        type.innerHTML='Type: '+c.type;
        type.classList.add('type');

        let value = document.createElement('div');
        value.innerHTML='Value: '+c.value;
        value.classList.add('value');
    

        conElem.append(name,type,value);
        
       return conElem;
    }
    showInfo(){
        document.querySelector('.contentContact').innerHTML='';
        let elem = this.contact.map((c)=>this.createInfo(c));
        document.querySelector('.contentContact').append(...elem);
    }
    update(){
        conServ.getAll().then(c=>{          
            this.contact = c;
            this.showContact(this.contact);
            this.showInfo(this.contact);
        }); 
    }
}
    let activeContact = null;

    document.addEventListener('click',(e)=>{               
    if(!e.target.matches('.showBtn')) return;
    let index = e.target.parentNode.dataset.index;
    activeContact = index;
    conttact.showContact();
    conttact.showInfo();
    activeContact=null;
}) 

