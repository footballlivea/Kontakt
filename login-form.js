class LoginForm{
    constructor(selector,userService){
        this.selector = selector;
        this.userService = userService; 
        this.onLogin = ()=>{}
        document.addEventListener(
            "DOMContentLoaded",
            ()=>{
                this.init();
                this.binds();
            }
        );
    }
    init(){
        this.form1 = document.querySelector('.unLoginScreen');
        this.form2 = document.querySelector('.LoginScreen');
        this.conteiner = document.querySelector(this.selector);
        this.loginInput = this.conteiner.querySelector('.form_log-input_login');
        this.passwordInput = this.conteiner.querySelector('.form_log-input_password');
        this.logBtn = this.conteiner.querySelector('button');
    }
    binds(){
        this.logBtn.addEventListener('click',()=>this.login());
    }
    login(){
        let user = new User(
            this.loginInput.value,
            null,
            this.passwordInput.value
        );
        this.userService.login(user).then(r=>{
            if(r.status === "error") this.loginError(r.error); 
            else sessionStorage.clear(); sessionStorage.setItem('token',r.token); this.loginSuccess();  
        })
    }
    loginError(text){
        alert(text);
        window.location.reload();
    }
    loginSuccess(){
        this.clearForm();
        this.onLogin();
    }
    clearForm(){
            this.loginInput.value=''
            this.passwordInput.value=''
    }
    hideS(){
        this.form1.style.display='none'
        this.form2.style.display='block'
    }
}
